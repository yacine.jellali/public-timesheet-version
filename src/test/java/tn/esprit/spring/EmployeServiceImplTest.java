package tn.esprit.spring;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.services.IContratService;
import tn.esprit.spring.services.IEmployeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeServiceImplTest {
	
	@Autowired
	EmployeRepository empRepository;
	@Autowired
	IEmployeService emp ; 
	@Autowired
	IContratService con ; 
	


	@Test
	public void testAuthenticate () {
		 Employe e = new Employe("larbi", "hedi","hedi.larbi@gmail.com" ,"hedi123",false,Role.TECHNICIEN);
		Employe Authentification = emp.authenticate(e.getEmail(), e.getPassword());

		assertEquals("hedi.larbi@gmail.com", Authentification.getEmail());
	}
	
	@Test
	public void testaddOrUpdateEmploye (){
		
		Employe e = new Employe("larbi ", "hedi ","hedi.larbi@gmail.com","hedi123",true,Role.TECHNICIEN);
		
		int employeAdded = emp.addOrUpdateEmploye(e);
		assertNotNull (employeAdded);
	}
	
	@Test
	public void testmettreAjourEmailByEmployeId (){
	  Employe e =new Employe() ;
       e.setEmail("hediTest@gmail.com");
	   e.setId(5);
       emp.mettreAjourEmailByEmployeId(e.getEmail(), e.getId());
     assertEquals(e.getEmail(),"hediTest2@gmail.com" );
     
	}
	
	
	@Test
	public void testgetAllEmployes(){
		List<Employe> employes  = emp.getAllEmployes() ;
		assertEquals(4,employes.size());
	}
	@Test
	public void testdelEmployes(){
		int empId =26 ;
		emp.deleteEmployeById(empId) ;
		Optional<Employe> findEmp = empRepository.findById(empId);
		if (findEmp.isPresent()) {
			fail("Non Supprimer");
		}else {
		assertTrue(true);
		}
	}
	
	@Test
	public void testAssignEmpToDep() {
		int empId = 1;
		int depId = 1;
		int EmpDep = 0;
		Optional<Employe> findEmp = empRepository.findById(empId);
		if (findEmp.isPresent()) {
			Employe empl = findEmp.get();
			emp.affecterEmployeADepartement(empId, depId);
			empl =  empRepository.findById(empId).get() ;
			int depNb = empl.getDepartements().size();
			for(int index = 0; index < depNb; index++)
				{
				if(empl.getDepartements().get(index).getId() == depId)
					{
					EmpDep = 1;
					break;
					}
				}
			assertEquals(EmpDep, 1);
		} else {
			fail("Employe with id " + empId + " not found");
		}
	}
	
	@Test
	public void testDeAssignEmpToDep() {
		int empId = 1;
		int depId = 1;
		int EmpDep = 0;
		Optional<Employe> findEmp = empRepository.findById(empId);
		if (findEmp.isPresent()) {
			emp.desaffecterEmployeDuDepartement(empId, depId);
			Employe empl = findEmp.get();
			int depNb = empl.getDepartements().size();
			for(int index = 0; index < depNb; index++)
				{
				if(empl.getDepartements().get(index).getId() == depId)
					{
					EmpDep = 1;
					break;
					}
				}
			assertEquals(0, EmpDep);
		} else {
			fail("Employe with id " + empId + " not found");
		}
	}
	
}
